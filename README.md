Pluggable Annotation Processing API Sample Client
===========================================================

Pluggable Annotation Processing API Core を拡張したサンプルAPIを使用するクライアントのサンプルです。

## LICENSE
Licensed under the [Apache License, Version 2.0][Apache]
[Apache]: http://www.apache.org/licenses/LICENSE-2.0
